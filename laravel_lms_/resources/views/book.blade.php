@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <button class="btn btn-primary mx-auto"><a href="{{ route('book.create') }}" class="text-white">Fill book Data</a></button>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-dark">
                            <thead>
                            <tr>
                                <th scope="col">author_id</th>
                                <th scope="col">title</th>
                                <th scope="col">pages</th>
                                <th scope="col">langauge</th>
                                <th scope="col">Cover_image</th>
                                <th scope="col">isbn</th>
                                <th scope="col">description</th>
                                <th scope="col">Status</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Updated At</th>
                                <th scope="col">Activity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($book as $bk)
                                <tr>
                                    <th scope="row">{{$bk->author_id}}</th>
                                    <th scope="row">{{$bk->title}}</th>
                                    <td>{{$bk->pages}}</td>
                                    <td>{{$bk->langauge}}</td>
                                    <td>{{$bk->cover_image }}</td>
                                    <td>{{$bk->isbn}}</td>
                                    <td>{{$bk->description}}</td>
                                    <td>{{$bk->status}}</td>
                                    <td>{{$bk->created_at}}</td>
                                    <td>{{$bk->updated_at}}</td>
                                    <td><a class="btn btn-primary" href="{{ route('book.edit',$bk->id) }}">Edit</a></td>
                                    <td>
                                        <form action="{{ route('book.destroy',$bk->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>

                                    </td>
                                    <td><button class="btn btn-info "><a href="{{route('book.show',$bk->id)}}">Show</a></button></td>

                                    <td>

                                        <form action="/active-deactive1" method="POST">
                                            @csrf
                                            <div class="input-group">
                                                <input type="hidden" class="form-control" name="v" value="{{$bk->id}}">
                                                <select required class="form-control" id="status"  name="status">
                                                    <option value="1">Active</option>
                                                    <option value="0">De-Active</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-primary">
                                                <span>Confirm</span>
                                            </button>

                                        </form>

{{--                                        @if($book->status==1)--}}
{{--                                            <a href="{{ route('statusBook',[$book->id]) }}" class="btn btn-danger">Inactive</a>--}}
{{--                                        @else--}}
{{--                                            <a href="{{ route('statusBook',[$book->id]) }}" class="btn btn-warning">active</a>--}}
{{--                                        @endif--}}


                                    </td>


                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

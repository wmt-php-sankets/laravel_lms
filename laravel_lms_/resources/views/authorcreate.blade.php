@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('author.store') }}" method="POST" id="myform" name="form">
                @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Full Name</label>
                <input type="text" name="fullname" id="firstname" class="form-control aa" id="firstname" aria-describedby="emailHelp" placeholder="Enter First Name">
                <span class="text-danger">  @error('fullname')
                        {{$message}}
                 @enderror
                </span>
            </div>



            <div class="form-group">
                <label for="exampleInputPassword1">Date-of-Birth</label>
                <input type="date" class="form-control" name="dob" id="dob" placeholder="Date-of-Birth">
                <span class="text-danger">  @error('dob')
                    {{$message}}
                    @enderror
                </span>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1"  class="pr-5">Select Your Gender</label>
                <input type="radio" name="gender" checked="checked" value="male"> Male
                <input type="radio" name="gender" value="female"> Female
                <input type="radio" name="gender" value="other"> Other
                <span class="text-danger">  @error('gender')
                    {{$message}}
                    @enderror
                </span>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Address</label><br>
                <input type="text" class="form-control" id="desc1" name="address_" placeholder="Enter your Address">
                <span class="text-danger">  @error('address_')
                    {{$message}}
                    @enderror
                </span>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Mobile No</label>
                <input type="number" class="form-control" id="m_number" name="mobile_no" placeholder="Enter Mobile Number">
                <span class="text-danger">  @error('mobile_no')
                    {{$message}}
                    @enderror
                </span>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Description</label><br>
                <textarea rows="4" cols="50" id="desc" name="description_">
                            data is not avalable
                    </textarea>
            </div>

            <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>

        </form>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">



        <form action="{{ route('book.store') }}" method="POST" id="myform" name="form" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="">Book Author</label>
                <select class="custom-select @error('book_author') is-invalid @enderror" name="author_id" id="author_id">
                    <option hidden value="">Book Author</option>
                    @foreach($author as $author)
                        <option value="{{ $author->id }}">{{ $author->fullname}}</option>
                    @endforeach
                </select>
                <span class="text-danger">
                @error('author_id')
                {{$message}}
                @enderror
                </span>
            </div>


            <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            <input type="text" name="title" id="title" class="form-control" aria-describedby="emailHelp" placeholder="Enter Book Title">
                <span class="text-danger">
                @error('title')
                    {{$message}}
                    @enderror
                </span>

            </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Pages</label>
            <input type="number" name="pages" id="pages" class="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Enter Pages">

            <span class="text-danger">
                @error('pages')
                {{$message}}
                @enderror
                </span>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">langauge</label>
            <input type="text" class="form-control" name="langauge" id="langauge" placeholder="Enter Book Langauge">

            <span class="text-danger">
                @error('langauge')
                {{$message}}
                @enderror
                </span>
        </div>







        <div class="form-group">
            <label for="exampleInputPassword1">Cover Image</label>
            <input type="file" class="form-control" id="image" name="cover_image" placeholder="Enter Cover Image">

            <span class="text-danger">
                @error('cover_image')
                {{$message}}
                @enderror
                </span>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">ISBN Number</label>
            <input type="text" class="form-control" name="isbn" id="isbn" placeholder="Isbn Number">
            <span class="text-danger">
                @error('isbn')
                {{$message}}
                @enderror
                </span>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Description</label><br>
            <textarea rows="4" cols="50" id="desc" name="description" placeholder="Description">
    </textarea>
            <span class="text-danger">
                @error('description')
                {{$message}}
                @enderror
                </span>
        </div>

        <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>

    </form>
    </div>
@endsection























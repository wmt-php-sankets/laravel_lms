@extends('layouts.app')

@section('content')

    <table class="table table-striped table-hover table-dark">
        <thead>
        <tr>
            <th scope="col">Author_id</th>
            <th scope="col">Title</th>
            <th scope="col">pages</th>
            <th scope="col">langauge</th>
            <th scope="col">cover_image</th>
            <th scope="col">isbn</th>
            <th scope="col">description</th>
            <th scope="col">status</th>
            <th scope="col">Created At</th>
            <th scope="col">Updated At</th>
            <th scope="col"><Activity></Activity></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">{{$book->author_id}}</th>
            <td>{{$book->title}}</td>
            <td>{{$book->pages}}</td>
            <td>{{$book->langauge }}</td>
            <td><img src="{{$book->cover_image  }}" height="100px" width="100px"></td>
            <td>{{$book->isbn}}</td>
            <td>{{$book->description}}</td>
            <td>{{$book->created_at}}</td>
            <td>{{$book->updated_at}}</td>
        </tr>

        </tbody>
    </table>
@endsection
